(() => {
    const teliaUrl = 'https://www.teliaplay.se/';
    const titleElem = document.querySelector('h1');
    const title = getTitleFromPage();
    let goToSearch = false;
    let directHitItem;

    if (searchForTitle(title)) {
        if (!directHitItem) {
            goToSearch = true;
        }

        appendTeliaPlayButton(goToSearch);
    }

    function appendTeliaPlayButton(goToSearch) {
        const linkElem = getLinkElem(goToSearch);

        titleElem.parentNode.insertBefore(linkElem, titleElem.nextSibling);
    }

    function getLinkElem(goToSearch) {
        const link = document.createElement('a');
        const teliaSearchTemplate = `${teliaUrl}search/all/{keyword}/all`;
        const stylingLink = ``;
        const imgElem = getImgElem();
        let url;

        if (goToSearch) {
            url = teliaSearchTemplate.replace('{keyword}', encodeURI(title));
        } else {
            url = getTitlePageUrl();
        }

        link.classList.add('telia-link');
        link.setAttribute('href', url);
        link.setAttribute('target', '_blank');
        link.setAttribute('style', stylingLink);
        link.innerText = 'Watch';
        link.prepend(imgElem);

        return link;
    }

    function getImgElem() {
        const imgElem = document.createElement('img');

        imgElem.setAttribute('src', chrome.runtime.getURL('images/favicon-32x32.png'));
        imgElem.classList.add('telia-link__btn');

        return imgElem;
    }

    function getTitlePageUrl() {
        const item = directHitItem;
        const isSeries = item.type === 'SeriesObject';
        const seriesUrl = `${teliaUrl}play/series/{storeId}/{id}`;
        const VodUrl = `${teliaUrl}store/video/{id}`;
        const SvodUrl = `${teliaUrl}play/{storeId}/program/{id}`;
        let targetUrl;

        if (isSeries) {
            targetUrl = seriesUrl.replace('{storeId}', item.object.storeId);
            targetUrl = targetUrl.replace('{id}', item.object.loopId);
        } else if (item.object.storeType === 'VOD') {
            targetUrl = VodUrl.replace('{id}', item.object.loopId);
        } else if (item.object.storeType === 'SVOD') {
            targetUrl = SvodUrl.replace('{storeId}', item.object.storeId);
            targetUrl = targetUrl.replace('{id}', item.object.id);
        }

        return targetUrl;
    }

    function getTitleFromPage() {
        const tempElem = document.createElement('textarea');
        let title = titleElem.innerHTML.split('<span')[0];

        tempElem.innerHTML = title;
        title = tempElem.innerText.trim();

        console.log('title', title);

        return title;
    }

    function searchForTitle(title) {
        const searchTemplate = `${teliaUrl}/loop54/v1/search?format=hls_sd&fromIndex=0&parental=true&playId=8930,8931,8932,8935,8936,8937,8938,8939,8940,8941,8942,8943,8944,8945,8946,8947,8948,8949,8950,8951,1348532111993,8954,8956,8957,8959,8964,8965,8966,8969,1348532111990,8970,8971,1895894284,1348532111991,8972,8973,8975,8976,1348532111984,8980,8982,8983,8984,8987,8988,8991,8992,8994,8995,8999,2821052379,8900,8901,8902,8906,8910,8913,8914,8917,8918,8919,8920,8921&search={keyword}&toIndex=50`;
        const searchKeyword = title;
        const searchUrl = searchTemplate.replace('{keyword}', searchKeyword);
        const xhr = new XMLHttpRequest();
        let result;
        let matchingItems;

        xhr.open('GET', searchUrl, false);
        xhr.send();

        try {
            result = JSON.parse(xhr.responseText);
            console.log('response', result.Data.directHits);
        } catch (e) {
            return false;
        }

        matchingItems = result.Data.directHits.filter(item => item.object.title && item.object.title.toLowerCase() === title.toLowerCase());

        if (matchingItems.length === 1) {
            console.log('direct hit!');
            console.log(matchingItems[0]);
            directHitItem = matchingItems[0];
        } else if (matchingItems.length > 1) {
            console.log('indirect hit!');
        } else {
            console.log('miss :(');
            return false;
        }

        return true;
    }
})();
