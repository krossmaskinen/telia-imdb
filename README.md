# Telia <3 IMDb

Chrome extension that shows a link to Telia on IMDb title pages if the title is available on Telia.

## IDEAS FOR IMPROVEMENTS

- Show Telia icon directly on search results
- Show Telia icon on all objects that are movies on a page (recommendations, swim lanes, etc.)
- Option to remove content that's not available on Telia
- ~~Link directly to title page on Telia (need to construct the url correctly)~~

## KNOWN BUGS

- Swedish (Telia) and English (IMDb) titles don't always match and results in no hits
